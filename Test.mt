(* Wolfram Language Test file *)

(* Homological EQUATION *)
Homological[expr_] := Module[{d, n},
	d = DecomposeDelaunay[expr];
	n = $a^(-3/2) $mu^(1/2);
	expr - d[[1]] - n Dt[d[[2]], $ell] // CanonicalForm // CollectRadius
];

HomologicalTest[expr_] := VerificationTest[ Homological[expr], 0, TestID -> "Homological " <> ToString[InputForm[expr]]];

NumericHomological[expr_, a_, e_, mu_] := Module[{A, B, H},
	{A, B} = DecomposeDelaunay[expr];
	H = expr - (A + $n Dt[B, $ell]);
	
	H = H /. $p -> $a(1-$e^2);
	H = H /. $beta -> 1/(1+$eta);
	H = H /. $eta -> Sqrt[1-$e^2];
	H = H /. $n -> $mu^(1/2) $a^(-3/2);
	
	H = H /. $phi -> $f - ($E - $e Sin[$E]);
	H = H /. $f -> 2 ArcTan[Sqrt[(1+$e)/(1-$e)] Tan[$E/2]];
	H = H /. $R -> $a^(1/2) $mu^(1/2) $e Sin[$E] / $r;
	H = H /. $r -> $a (1 - $e Cos[$E]);
	
	Print[H];
	
	NIntegrate[Norm[H] /. {$e -> e, $a -> a, $mu -> mu}, {$E, -Pi, Pi}, WorkingPrecision -> 100, AccuracyGoal -> 10]
];

NumericHomologicalTest[expr_, a_, e_, mu_] := VerificationTest[ NumericHomological[expr, a, e, mu], 0, SameTest -> (Abs[#1 - #2] < 10^(-6) &), TestID -> "Numeric " <> ToString[InputForm[expr]] <> " (a=" <> ToString[a] <> ",e=" <> ToString[e] <> ",mu=" <> ToString[mu] <> ")"];
NumericHomologicalR[expr_] := NumericHomologicalTest[expr, Random[], Random[], Random[]];


NumericHomologicalR[$r^-5 PolyLog[4, -$beta $e E^(I $f)] E^(2I $f)];

(*
HomologicalTest[1/$r^2];
HomologicalTest[1/$r];
HomologicalTest[$r];
HomologicalTest[$r^20];
HomologicalTest[1/$r^20];

HomologicalTest[$R];
HomologicalTest[$R $r];
HomologicalTest[$R $r^20];
HomologicalTest[$R/$r];
HomologicalTest[$R/$r^20];

HomologicalTest[Cos[$E]];
HomologicalTest[Sin[$E]];
HomologicalTest[Cos[$f]];
HomologicalTest[Sin[$f]];

NumericHomologicalTest[$phi $R/$r, 0.2, 0.3, 0.9];
NumericHomologicalTest[$phi $R/$r, 0.8, 1.2, 0.3];
NumericHomologicalTest[$phi $R/$r, 0.5, 0.54, 0.7];
NumericHomologicalTest[$phi $R $r, 0.4, 0.35, 0.5];
NumericHomologicalTest[$phi $R $r^3, 0.4, 0.35, 0.5];

NumericHomologicalTest[$phi, 0.4, 0.6, 0.8];
NumericHomologicalTest[$phi, 0.7, 0.9, 0.1];
NumericHomologicalTest[$phi/$r, 0.4, 0.6, 0.8];
NumericHomologicalTest[$phi/$r, 0.7, 0.9, 0.1];
NumericHomologicalTest[$phi/$r^2, 0.4, 0.6, 0.8];
NumericHomologicalTest[$phi/$r^2, 0.7, 0.9, 0.1];
NumericHomologicalTest[$phi/$r^4, 0.7, 0.9, 0.1];
NumericHomologicalTest[$phi/$r^4, 0.7, 0.9, 0.1];
NumericHomologicalTest[$phi $r^2, 0.7, 0.9, 0.1];
NumericHomologicalTest[$phi $r^2, 0.7, 0.9, 0.1];

*)