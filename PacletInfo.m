(* Paclet Info File *)

(* created 2023.10.22*)

Paclet[
  Name -> "DelaunayNormalization",
  Version -> "0.0.1",
  WolframVersion -> "6+",
  Extensions -> {
    {"Documentation", Language -> "English"}
}]