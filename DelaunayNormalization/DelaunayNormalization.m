(* Wolfram Language Package *)

(* Created by the Wolfram Workbench Oct 22, 2023 *)

BeginPackage["DelaunayNormalization`"]

(* Exported symbols added here with SymbolName::usage *)

DecomposeDelaunay::usage = "DecomposeDelaunay[f] Decomposes the function f into the Kernel and Image of the Delaunay Lie Operator $Lo";
DecompositionNotImplemented::usage = "Symbol that is returned when a decomposition is not implemented";

(* Variables definition *)
$f::usage = "True anomaly";
$E::usage = "Eccentric anomaly";
$phi::usage = "Center";
$ell::usage = "Main anomaly";
$r::usage = "Radius";
$R::usage = "Radius conjugate";

(* Constants definition *)
$Theta::usage = "Length of angular momentum";
$a::usage = "Semi-major Axis";
$e::usage = "Eccentricity";
$p::usage = "Semi-Latus rectum";
$n::usage = "Mean motion";
$eta::usage = "";
$beta::usage = "";
$G::usage = "";

$mu::usage = "Small parameter";

ConvertConstants::usage = "";
ConvertToE::usage = "";

ExpandedQ::usage = "";
FreeEllQ::usage = "";
AllzEQ::usage = "";

Begin["`Private`"]
(* Implementation of the package *)

DecompositionNotImplemented /: Plus[DecompositionNotImplemented, _] = DecompositionNotImplemented;
DecompositionNotImplemented /: Times[DecompositionNotImplemented, _] = DecompositionNotImplemented;

FreeEllQ[expr_] := FreeQ[expr, $r] && FreeQ[expr, $R] && FreeQ[expr, $E] && FreeQ[expr, $f] && FreeQ[expr, $phi] && FreeQ[expr, zE] && FreeQ[expr, zf] && FreeQ[expr, polylogze] && FreeQ[expr, polylogzf];
ExpandedQ[expr_] := SameQ[expr, Expand[expr]];

(* Partial derivatives *)

$r /: Dt[$r, $ell] = $a^(3/2) $mu^(-1/2) $R;
$R /: Dt[$R, $ell] = $a^(5/2) $mu^(1/2) (1-$e^2) / $r^3 - $a^(3/2) $mu^(1/2) / $r^2;
$E /: Dt[$E, $ell] = $a / $r;
$f /: Dt[$f, $ell] = $a^2 Sqrt[1 - $e^2] / $r^2;
$phi /: Dt[$phi, $ell] = $a^2 Sqrt[1 - $e^2] / $r^2 - 1;

$e /: Dt[$e, $ell] = 0;
$a /: Dt[$a, $ell] = 0;

SetAttributes[$mu, Constant];

(* Anomalies to Radius conversion *)

Clear[AnomaliesToRadius];
Clear[DecomposeDelaunay];
Clear[DecomposeDelaunayImpl];
Clear[PowersOfR];

DecomposeDelaunay[list_List] := DecomposeDelaunay /@ list;
DecomposeDelaunay[terms_Plus] := DecomposeDelaunay /@ terms;
DecomposeDelaunay[expr_] := DecomposeDelaunay[expr // Expand] /; Not[ExpandedQ[expr]];
DecomposeDelaunay[expr_] := DecomposeDelaunay[expr /. {
	PolyLog[i_, beta_. Exp[Complex[0, m_] $E]] :> polylogze[i, beta, m],
	Log[1 - beta_. Exp[Complex[0, m_] $E]] :> -polylogze[1, beta, m],
	
	PolyLog[i_, beta_. Exp[Complex[0, m_] $f]] :> polylogzf[i, beta, m],
	Log[1 - beta_. Exp[Complex[0, m_] $f]] :> -polylogzf[1, neta, m],
	
	Log[beta_. Exp[Complex[0, m_] $E]] :> logze[beta, m],
	Log[beta_. Exp[Complex[0, m_] $f]] :> logzf[beta, m],
	
	Log[c_./$r] :> logr[c]
	
}] /; Not[FreeQ[expr, PolyLog]] || Not[FreeQ[expr, Log]];

DecomposeDelaunay[expr_] := DecomposeDelaunay[(expr // ExpToTrig // TrigExpand) /. {
	Cos[$f] -> 1/$e ($p/$r - 1),
	Sin[$f] -> $eta/($n $a $e) $R,
	Cos[$E] -> 1/$e (1 - $r/$a),
	Sin[$E] -> 1/($n $a^2 $e) $r $R
}] /; Not[FreeQ[expr, $E]] || Not[FreeQ[expr, $f]];
DecomposeDelaunay[expr_] := DecomposeDelaunay[expr /. {
	Power[$R, m_] /; EvenQ[m] && m > 0 :> Expand[(-$mu/$a + 2 $mu /$r - $a $mu (1-$e^2) / $r^2 )^(m/2)],
	Power[$R, m_] /; OddQ[m] && m > 1 :> Expand[$R (-$mu/$a + 2 $mu /$r - $a $mu (1-$e^2) / $r^2 )^((m-1)/2)]
}] /; Not[FreeQ[expr, Power[$R, m_]]];
 
DecomposeDelaunay[c_] := {c, 0} /; FreeEllQ[c];
DecomposeDelaunay[c_ f_] := c DecomposeDelaunay[f] /; FreeEllQ[c];

(* R r^k *)
DecomposeDelaunay[$R/$r] := {0, -Log[$a(1-$e^2)/$r]};
DecomposeDelaunay[$R Power[$r, n_.]] := ( DecomposeDelaunay[$R Power[$r, n]] = {0, $r^(n+1)/(n+1)} ) /; n != -1 && FreeEllQ[n];
DecomposeDelaunay[$R] := {0, $r};

(* r^k *)
DecomposeDelaunay[Power[$r, n_.]] := ( DecomposeDelaunay[Power[$r, n]] = Expand[(2n+5)/($a (1-$e^2) (n+2)) DecomposeDelaunay[$r^(n+1)]] - Expand[(3+n)/($a^2 (1 - $e^2) (n+2)) DecomposeDelaunay[$r^(n+2)]] - {0, 1/($a (1-$e^2) (n+2) $mu) $R $r^(n+3)} ) /; FreeEllQ[n] && n < -2;
DecomposeDelaunay[Power[$r, n_.]] := ( DecomposeDelaunay[Power[$r, n]] = Expand[$a (2n+1)/(n+1) DecomposeDelaunay[$r^(n-1)]] - Expand[$a^2 n (1 - $e^2)/(n+1) DecomposeDelaunay[$r^(n-2)]] - {0, $a/((n+1) $mu) $R $r^(n+1)} ) /; FreeEllQ[n] && n >= 0;
DecomposeDelaunay[1/$r^2] := {1/($a^2 Sqrt[1-$e^2]), 1/($a^(1/2) Sqrt[1 - $e^2] $mu^(1/2)) $phi};
DecomposeDelaunay[1/ $r] := {1/$a, 1/$mu $R $r};
DecomposeDelaunay[$r] := {$a ($e^2+2)/2, $a^2 ($e^2 - 1)/(2 $mu) $R $r - $a/(2 $mu) $R $r^2};
DecomposeDelaunay[Power[$r, n_]] := {$a^n (1+$e)^(n+1) Hypergeometric2F1[1/2, -n-1, 1, 2$e/(1+$e)], 2 $a^(3/2+n) (1-$e)^(1+n) Sin[$E/2] AppellF1[1/2, 1/2, -n-1, 3/2, (1-Cos[$E])/2, $e(1-Cos[$E])/2] + $a^(3/2+n) (1+$e)^(n+1) Hypergeometric2F1[1/2, -n-1, 1, 2$e/(1+$e)] ($e Sin[$E] - $E) };

(*
	Int[phi^m f dl]
	u = phi               dv = phi^(m-1) f = A + L[B]
	du = G/(n r^2) - 1     v = l A + n B
	
	Int[phi^m f dl] = phi l A + n phi B - Int[phi' A l dl] - n Int[phi' B dl]
	phi^m f = phi A + phi' l A + L[phi B] - phi' A l - n phi' B
	phi^m f = phi A + L[phi B] - n(G/(n r^2) - 1) B
	phi^m f = phi A + L[phi B] - (G/r^2 - n) B
	phi^m f = phi A + L[phi B] - (G B/r^2 - n B)
	phi^m f = phi A + L[phi B] - G B/r^2 + n B
*)
DecomposeDelaunay[$phi] := Expand[$a^(3/2) $mu^(-1/2) Sqrt[1-$e^2] DecomposeDelaunay[$R/$r]] - Expand[1/($a^(1/2) $mu^(1/2)) DecomposeDelaunay[$R $r]] + Expand[$a DecomposeDelaunay[$phi/$r]] - {0, $a/$mu $phi $R $r};
DecomposeDelaunay[$phi/$r^2] := {0, 1 / (2 $a^(1/2) $mu^(1/2) Sqrt[1-$e^2]) $phi^2 + 1/($a^(3/2) $mu^(1/2) Sqrt[1-$e^2]) $r - 1/($a $mu Sqrt[1-$e^2]) $phi $R $r - 1/(2 $a^(5/2) $mu^(1/2) Sqrt[1-$e^2]) $r^2 - 1 / ($a^(1/2) $mu^(1/2)) Log[$a (1-$e^2) /$r] - 1 / ($a^(1/2) $mu^(1/2) Sqrt[1-$e^2]) (PolyLog[2, $e/(1 + Sqrt[1-$e^2]) Exp[-I $E]] + PolyLog[2, $e/(1 + Sqrt[1-$e^2]) Exp[I $E]])};
DecomposeDelaunay[$phi/$r] := {0, -1/($n $a) PolyLog[2, $beta $e Exp[-I $E]] - 1/($n $a) PolyLog[2, $beta $e Exp[I $E]] + 1/($n $a^2) $r};
DecomposeDelaunay[Power[$phi, m_.] f_] := Module[{A, B},
	{A,B} = DecomposeDelaunay[$phi^(m-1) f];
	Print[$phi^(m-1) f, " -> ", A, " -> ", B];
	{0, $phi B} + A DecomposeDelaunay[$phi] - $G DecomposeDelaunay[B/$r^2] + $n DecomposeDelaunay[B]
];

(*
	polylogzf[i, beta, m] = PolyLog[i, beta Exp[I m f]]

	Int[polylogzf[i, beta, m]  f dl]
	u = polylogzf[i, beta, m]
	du = I m $a^2 $eta polylogzf[i-1, beta, m]/$r^2
	dv = f = A + L[B]
	v = l A + n B
	
	Int[polylogzf f dl] = u A l + n u B - Int[du A l dl] - n Int[du B dl]
	polylogzf f = du A l + u A + L[u B] - du A l - n du B
	polylogzf = u A + L[u B] - n du B
	polylogzf = polylogzf[i, beta, m] A + L[polylogzf[i, beta, m] B] - I n m $a^2 $eta polylogzf[i-1, beta, m] B/$r^2
*)
PowerzfQ[expr_] := FreeEllQ[expr] || MatchQ[expr, c_. zf^i_. /; FreeEllQ[c]];
AllzfQ[expr_] := AllTrue[expr, PowerzfQ];

DecomposeDelaunay[polylogzf[i_, beta_, m_] f_] := Module[{A, B, fzf},
	{A, B} = DecomposeDelaunay[f];
	
	
	fzf = f * (- 4 I $eta^3 zf / ($e + 2zf + $e zf^2)^2);
	fzf = fzf /. $phi -> $f - $ell;
	fzf = fzf /. $ell -> I ($e Sqrt[1-$e^2] (zf^2-1)/($e zf^2 + 2zf + $e) + Log[($e zf + 1 + Sqrt[1-$e^2])/((1+Sqrt[1-$e^2])zf + $e)]);
	fzf = fzf /. $f -> -I Log[zf];
	fzf = fzf /. $E -> -I Log[($e zf + 1 + Sqrt[1-$e^2])/((1 + Sqrt[1-$e^2]) zf + $e)];
	fzf = fzf /. $r -> 2 $a (1-$e^2) zf/($e zf^2 + 2zf + $e);
	fzf = fzf /. $R -> I $e/(2 Sqrt[1-$e^2]) $mu^(1/2) $a^(-1/2) (1-zf^2)/zf ;
	fzf = fzf // Expand;
	
	Print["zf: a ", fzf];
	
	
	If[AllzfQ[fzf],
		Print["zf: ", f, " -> ", fzf];
		{0, zftopolyint[fzf, i, beta, m]},
		Print["zf: ", f, " -> ", A, " -> ", B];
		A DecomposeDelaunay[PolyLog[i, beta Exp[I m $f]]] + {0, PolyLog[i, beta Exp[I m $f]] B} - I $n m $a^2 $eta DecomposeDelaunay[PolyLog[i-1, beta Exp[I m $f]] B/$r^2]	]
];

zftopolyint[p_Plus, i_, beta_, m_] := zftopolyint[#, i, beta, m]& /@ p;
zftopolyint[c_. Power[zf, q_.], i_, beta_, m_] := c PolyInt[q, i, beta, Exp[I m $f]] /; FreeEllQ[c];
zftopolyint[c_, i_, beta_, m_] := c PolyInt[0, i, beta, Exp[I m $f]] /; FreeEllQ[c];

PolyInt[q_, p_, s_, w_] := - Log[1 - s w]/(s^(q+1)(-q-1)^p) + w^(q+1)/(-q-1)^p (-Sum[(s w)^(i-q-1)/i, {i, 1, q+1}] + Sum[(-1)^i(q+1)^(i-1) PolyLog[i, s w], {i, 1, p}]) /; q >= 0;
PolyInt[-1, p_, s_, w_] := PolyLog[p+1, s w];
PolyInt[-2, p_, s_, w_] := s(Log[s w] - Log[1 - s w]) - 1/w Sum[PolyLog[i, s w], {i, 1, p}];
PolyInt[q_, p_, s_, w_] := (Log[s w] - Log[1 - s w])/(s^(q+1)(-q-1)^p) + w^(q+1)/(-q-1)^p (Sum[(s w)^i/(q+1+i), {i, 1, -q-2}] + Sum[(-1)^i(q+1)^(i-1)PolyLog[i, s w], {i, 1, p}]) /; q <= -3;


DecomposeDelaunay[expr_] := Module[{},
	Print["Decomposition of ", expr, " not implemented"];
	{DecompositionNotImplemented,DecompositionNotImplemented}
];


(*

(* TODO: acabar las constantes *)
ConstantsDict = <|
	$a -> {$p -> $a (1-$e^2), $n -> $mu^(1/2) $a^(-3/2)},
	$p -> {$a -> $p/(1-$e^2)},
	$e -> {$p -> $a (1-$e^2), $eta -> Sqrt[1-$e^2], $beta -> 1 + Sqrt[1-$e^2]}
|>;

ConvertConstants[expr_, resultvars_List] := expr /. Flatten[Map[ConstantsDict[#]&, resultvars]];

ConvertToRadius[expr_] := (expr // ExpToTrig // TrigExpand) /.
	{
		Cos[$f] -> $a (1/$e - $e) 1/$r - 1/$e,
		Sin[$f] -> $a^(1/2) $mu^(-1/2) Sqrt[1 - $e^2] / $e $R,
		Cos[$E] -> - 1/($e $a) $r + 1/$e,
		Sin[$E] -> $a^(-1/2) $mu^(-1/2)/$e $r $R
	} // Expand;

ConvertToE[expr_] := ((((expr /.
	$phi -> $f - ($E - $e Sin[$E]) )/.
	$f -> 2 ArcTan[Sqrt[(1+$e)/(1-$e)] Tan[$E/2]] )/.
	$R -> $a^(1/2) $mu^(1/2) $e Sin[$E] / $r )/.
	$r -> $a (1 - $e Cos[$E]) );

ConvertTof[expr_] := ((((expr /.
	$phi -> $f - ($E - $e Sin[$E]) )/.
	$E -> ArcTan[(Sqrt[1-$e^2] Sin[$f])/($e + Cos[$f])] )/.
	$R -> $e $mu^(1/2) $a^(-1/2)/Sqrt[1-$e^2] Sin[$f] )/.
	$r -> $a (1-$e^2)/(1+$e Cos[$f]) );

CanonicalForm[expr_] := AnomaliesToRadius[ConstantsToAE[expr]] /. {
	Power[$R, n_] /; EvenQ[n] && n > 0 :> Expand[(-$mu/$a + 2 $mu /$r - $a $mu (1-$e^2) / $r^2 )^(n/2)], (* TODO: Add mu *)
	Power[$R, n_] /; OddQ[n] && n > 1 :> Expand[$R (-$mu/$a + 2 $mu /$r - $a $mu (1-$e^2) / $r^2 )^((n-1)/2)]
} // Expand;



DecomposeDelaunay[list_List] := DecomposeDelaunay /@ list;
DecomposeDelaunay[expr_] := DecomposeDelaunayA[expr // ExpandAll];
DecomposeDelaunayA[terms_Plus] := DecomposeDelaunayA /@ terms;
DecomposeDelaunayA[c_ f_] := c DecomposeDelaunayA[f] /; FreeEllQ[c];
DecomposeDelaunayA[expr_] := DecomposeDelaunayPhi[expr];

(* Handle expressions of the form $phi^m f *)
(*
	Int[phi^m f dl]
	u = phi               dv = phi^(m-1) f = A + L[B]
	du = phi'              v = l A + n B
	Int[phi^m f dl] = phi l A + n phi B - Int[phi' A l dl] - n Int[phi' B dl]
	n phi^m f = L[phi l A] + n L[phi B] - n phi' A l - n^2 phi' B
	n phi^m f = n phi' A l + n A phi + n L[phi B] - n phi' A l - n^2 phi' B
	phi^m f = L[phi B] + A phi - n phi' B 
	phi^m f = L[phi B] + A phi - n $a^2 Sqrt[1 - $e^2] B / $r^2 - B;
*)
DecomposeDelaunayPhi[$phi] := Expand[$a^(3/2) $mu^(-1/2) Sqrt[1-$e^2] DecomposeDelaunayImpl[$R/$r]] - Expand[1/($a^(1/2) $mu^(1/2)) DecomposeDelaunayImpl[$R $r]] + Expand[$a DecomposeDelaunayImpl[$phi/$r]] - {0, $a/$mu $phi $R $r};
DecomposeDelaunayPhi[$phi/$r^2] := {0, 1 / (2 $a^(1/2) $mu^(1/2) Sqrt[1-$e^2]) $phi^2 + 1/($a^(3/2) $mu^(1/2) Sqrt[1-$e^2]) $r - 1/($a $mu Sqrt[1-$e^2]) $phi $R $r - 1/(2 $a^(5/2) $mu^(1/2) Sqrt[1-$e^2]) $r^2 - 1 / ($a^(1/2) $mu^(1/2)) Log[$a (1-$e^2) /$r] - 1 / ($a^(1/2) $mu^(1/2) Sqrt[1-$e^2]) (PolyLog[2, $e/(1 + Sqrt[1-$e^2]) Exp[-I $E]] + PolyLog[2, $e/(1 + Sqrt[1-$e^2]) Exp[I $E]])};
DecomposeDelaunayPhi[Power[$phi, m_.] f_] := Module[{},
	{A,B} = DecomposeDelaunay[$phi^(m-1) f];
	{0, $phi B} + A DecomposeDelaunayPhi[$phi] + $mu^(1/2) $a^(-3/2) DecomposeDelaunay[B] - $a^(1/2) $mu^(1/2) Sqrt[1-$e^2] DecomposeDelaunay[B/$r^2]
];
DecomposeDelaunayPhi[expr_] := DecomposeDelaunayPolyLog[expr];

DecomposeDelaunayPolyLog[PolyLog[n_, pl_] f_] := {DecompositionNotImplemented, DecompositionNotImplemented};
DecomposeDelaunayPolyLog[expr_] := DecomposeDelaunayLog[expr];

DecomposeDelaunayLog[Log[a_] f_] := {DecompositionNotImplemented, DecompositionNotImplemented};
DecomposeDelaunayLog[expr_] := DecomposeDelaunayRr[expr /. {
	Power[$R, n_] /; EvenQ[n] && n > 0 :> Expand[(-$mu/$a + 2 $mu /$r - $a $mu (1-$e^2) / $r^2 )^(n/2)],
	Power[$R, n_] /; OddQ[n] && n > 1 :> Expand[$R (-$mu/$a + 2 $mu /$r - $a $mu (1-$e^2) / $r^2 )^((n-1)/2)]

}];

(* R r^k *)
DecomposeDelaunayRr[$R/$r] := {0, -Log[$a(1-$e^2)/$r]};
DecomposeDelaunayRr[$R Power[$r, n_.]] := ( DecomposeDelaunayImpl[$R Power[$r, n]] = {0, $r^(n+1)/(n+1)} ) /; Not[SameQ[n, -1]] && FreeEllQ[n];
DecomposeDelaunayRr[$R] := {0, $r};

(* r^k *)
DecomposeDelaunayRr[Power[$r, n_]] :=. ( DecomposeDelaunayImpl[Power[$r, n]] = Expand[(2n+5)/($a (1-$e^2) (n+2)) DecomposeDelaunayImpl[$r^(n+1)]] - Expand[(3+n)/($a^2 (1 - $e^2) (n+2)) DecomposeDelaunayImpl[$r^(n+2)]] - {0, 1/($a (1-$e^2) (n+2) $mu) $R $r^(n+3)} ) /; FreeEllQ[n] && n < -2; (* TODO: Add mu *)
DecomposeDelaunayRr[Power[$r, n_]]ore := ( DecomposeDelaunayImpl[Power[$r, n]] = Expand[$a (2n+1)/(n+1) DecomposeDelaunayImpl[$r^(n-1)]] - Expand[$a^2 n (1 - $e^2)/(n+1) DecomposeDelaunayImpl[$r^(n-2)]] - {0, $a/((n+1) $mu) $R $r^(n+1)} ) /; FreeEllQ[n] && n >= 0;
DecomposeDelaunayRr[1/$r^2] := {1/($a^2 Sqrt[1-$e^2]), 1/($a^(1/2) Sqrt[1 - $e^2] $mu^(1/2)) $phi};
DecomposeDelaunayRr[1/ $r] := {1/$a, 1/$mu $R $r};
DecomposeDelaunayRr[$r] := {$a ($e^2+2)/2, $a^2 ($e^2 - 1)/(2 $mu) $R $r - $a/(2 $mu) $R $r^2};
DecomposeDelaunayRr[Power[$r, n_]] := {$a^n (1+$e)^(n+1) Hypergeometric2F1[1/2, -n-1, 1, 2$e/(1+$e)], 2 $a^(3/2+n) (1-$e)^(1+n) Sin[$E/2] AppellF1[1/2, 1/2, -n-1, 3/2, (1-Cos[$E])/2, $e(1-Cos[$E])/2] + $a^(3/2+n) (1+$e)^(n+1) Hypergeometric2F1[1/2, -n-1, 1, 2$e/(1+$e)] ($e Sin[$E] - $E) };

DecomposeDelaunayRr[expr_] := Module[{},
	Print["Decomposition of ", expr, " not implemented"];
	{DecompositionNotImplemented,DecompositionNotImplemented}
];





(*
DecomposeDelaunay[c_ Log[b_]] := DecomposeDelaunay[CanonicalForm[c] Log[b]] /; Not[CanonicalQ[c]];
DecomposeDelaunay[c_ PolyLog[a_, b_]] := DecomposeDelaunay[CanonicalForm[c] PolyLog[a, b]] /; Not[CanonicalQ[c]];
DecomposeDelaunay[expr_] := DecomposeDelaunay[CanonicalForm[expr]] /; Not[CanonicalQ[expr]] && FreeQ[expr, Log] && FreeQ[expr, PolyLog];
DecomposeDelaunay[expr_] := DecomposeDelaunayImpl[expr]; (* CollectRadius /@ DecomposeDelaunayImpl[expr]; *)
*)

(*
DecomposeDelaunayImpl[$zE] := {-$e Pi, -I/2 Pi $a^(3/2) $mu^(-1/2) $e^2 1/$zE + I/2 $a^(3/2) $mu^(-1/2) (Pi $e^2 - 2) $zE + I/4 $a^(3/2) $mu^(-1/2) $e $zE^2 + I/2 $a^(3/2) $mu^(-1/2) $e (1-2Pi) Log[$zE]};
DecomposeDelaunayImpl[1/$zE] := {-$e Pi, -I/4 $a^(3/2) $mu^(-1/2) $e 1/$zE^2 - I/2 $a^(3/2) $mu^(-1/2) (Pi $e^2 - 2) 1/$zE + I/2 Pi $a^(3/2) $mu^(-1/2) $e^2 $zE + I/2 $a^(3/2) $mu^(-1/2) $e (1-2Pi) Log[$zE]};
DecomposeDelaunayImpl[Power[$zE, ga_.]] := $mu^(-1/2) $a^(3/2) ( -I/ga {0, $zE^ga} + I $e/(2 (ga+1)) {0, $zE^(ga+1)} + I $e/(2 (ga-1)) {0, $zE^(ga-1)} ) /; ga != -1 && ga != 1;

DecomposeDelaunayImpl[Power[$zE, m_.] PolyLog[n_, beta_. $zE]] := Module[{II},
	II = I $e/2 zP[m, n, beta] + I $e/2 zP[m-2, n, beta] - I zP[m-1, n, beta];
	{0, $a^(3/2) $mu^(-1/2) II} // Expand
];
*)



(* R r^k *)
DecomposeDelaunayImpl[$R/$r] := {0, -Log[$a(1-$e^2)/$r]};
DecomposeDelaunayImpl[$R Power[$r, n_.]] := ( DecomposeDelaunayImpl[$R Power[$r, n]] = {0, $r^(n+1)/(n+1)} ) /; Not[SameQ[n, -1]] && FreeEllQ[n];
DecomposeDelaunayImpl[$R] := {0, $r};

(* r^k *)
DecomposeDelaunayImpl[Power[$r, n_]] := ( DecomposeDelaunayImpl[Power[$r, n]] = Expand[(2n+5)/($a (1-$e^2) (n+2)) DecomposeDelaunayImpl[$r^(n+1)]] - Expand[(3+n)/($a^2 (1 - $e^2) (n+2)) DecomposeDelaunayImpl[$r^(n+2)]] - {0, 1/($a (1-$e^2) (n+2) $mu) $R $r^(n+3)} ) /; FreeEllQ[n] && n < -2; (* TODO: Add mu *)
DecomposeDelaunayImpl[Power[$r, n_]] := ( DecomposeDelaunayImpl[Power[$r, n]] = Expand[$a (2n+1)/(n+1) DecomposeDelaunayImpl[$r^(n-1)]] - Expand[$a^2 n (1 - $e^2)/(n+1) DecomposeDelaunayImpl[$r^(n-2)]] - {0, $a/((n+1) $mu) $R $r^(n+1)} ) /; FreeEllQ[n] && n >= 0;
DecomposeDelaunayImpl[1/$r^2] := {1/($a^2 Sqrt[1-$e^2]), 1/($a^(1/2) Sqrt[1 - $e^2] $mu^(1/2)) $phi};
DecomposeDelaunayImpl[1/ $r] := {1/$a, 1/$mu $R $r};
DecomposeDelaunayImpl[$r] := {$a ($e^2+2)/2, $a^2 ($e^2 - 1)/(2 $mu) $R $r - $a/(2 $mu) $R $r^2};
DecomposeDelaunayImpl[Power[$r, n_]] := {$a^n (1+$e)^(n+1) Hypergeometric2F1[1/2, -n-1, 1, 2$e/(1+$e)], 2 $a^(3/2+n) (1-$e)^(1+n) Sin[$E/2] AppellF1[1/2, 1/2, -n-1, 3/2, (1-Cos[$E])/2, $e(1-Cos[$E])/2] + $a^(3/2+n) (1+$e)^(n+1) Hypergeometric2F1[1/2, -n-1, 1, 2$e/(1+$e)] ($e Sin[$E] - $E) };

(* phi R r^k *)
DecomposeDelaunayImpl[$phi $R / $r] := {$a^(-3/2) $mu^(1/2) (1 - Sqrt[1-$e^2] - Log[4 (1 - $e^2)/(1 + Sqrt[1-$e^2])^2]), - $phi Log[$a (1-$e^2) /$r] - (Sqrt[1-$e^2] + Log[2/(1 + Sqrt[1-$e^2])]) $phi + (2 - Log[2 (1-$e^2) / (1 + Sqrt[1-$e^2])]) / ($a^(1/2) $mu^(1/2)) $r $R + $a^(-1/2) $mu^(-1/2) $R $r Log[$a (1-$e^2)/$r] - I PolyLog[2, $e/(1 + Sqrt[1-$e^2]) Exp[-I $E]] + I PolyLog[2, $e/(1 + Sqrt[1-$e^2]) Exp[I $E]] - I PolyLog[2, -$e/(1 + Sqrt[1-$e^2]) / $zf] + I PolyLog[2, -$e/(1 + Sqrt[1-$e^2]) $zf]};
DecomposeDelaunayImpl[$phi $R Power[$r, n_.]] := $mu^(1/2)/($a^(3/2) (n+1)) DecomposeDelaunayImpl[$r^(n+1)] - $a^(1/2) $mu^(1/2) Sqrt[1-$e^2]/(n+1) DecomposeDelaunayImpl[$r^(n-1)] + {0, 1/(n+1) $phi $r^(n+1)} /; FreeEllQ[n];
DecomposeDelaunayImpl[$phi $R] := $a^(-3/2) $mu^(1/2) DecomposeDelaunayImpl[$r] - $a^(1/2) $mu^(1/2) Sqrt[1 - $e^2] DecomposeDelaunayImpl[1/$r] + {0, $phi $r};

(* phi r^k *)
DecomposeDelaunayImpl[$phi Power[$r, n_.]] := ( DecomposeDelaunayImpl[$phi Power[$r, n]] = Expand[1/($a^(1/2) $mu^(1/2) Sqrt[1-$e^2] (n+2)) DecomposeDelaunayImpl[$R $r^(n+1)]] - Expand[1/((n+2) $a^(5/2) $mu^(1/2) (1-$e^2)) DecomposeDelaunayImpl[$R $r^(n+3)]] + Expand[(2n+5) / ($a (1-$e^2) (n+2)) DecomposeDelaunayImpl[$phi $r^(n+1)]] - Expand[(n+3) / ($a^2 (1-$e^2) (n+2)) DecomposeDelaunayImpl[$phi $r^(n+2)]] - {0, 1/($a $mu (1-$e^2) (n+2)) $phi $R $r^(n+3)} ) /; FreeEllQ[n] && n < -2;
DecomposeDelaunayImpl[$phi Power[$r, n_.]] := ( DecomposeDelaunayImpl[$phi Power[$r, n]] = Expand[$a^(3/2) Sqrt[1-$e^2] $mu^(-1/2) / (n+1) DecomposeDelaunayImpl[$R $r^(n-1)]] - Expand[1/($a^(1/2) $mu^(1/2) (n+1)) DecomposeDelaunayImpl[$R $r^(n+1)]] - Expand[$a^2 (1-$e^2) n/(n+1) DecomposeDelaunayImpl[$phi $r^(n-2)]] + Expand[$a (2n+1)/(n+1) DecomposeDelaunayImpl[$phi $r^(n-1)]] - {0, $a/($mu (n+1)) $phi $R $r^(n+1)} ) /; FreeEllQ[c] && FreeEllQ[n] && n > -1;
DecomposeDelaunayImpl[$phi] := Expand[$a^(3/2) $mu^(-1/2) Sqrt[1-$e^2] DecomposeDelaunayImpl[$R/$r]] - Expand[1/($a^(1/2) $mu^(1/2)) DecomposeDelaunayImpl[$R $r]] + Expand[$a DecomposeDelaunayImpl[$phi/$r]] - {0, $a/$mu $phi $R $r};
DecomposeDelaunayImpl[$phi $r^(-2)] := {0, 1 / (2 $a^(1/2) $mu^(1/2) Sqrt[1-$e^2]) $phi^2 + 1/($a^(3/2) $mu^(1/2) Sqrt[1-$e^2]) $r - 1/($a $mu Sqrt[1-$e^2]) $phi $R $r - 1/(2 $a^(5/2) $mu^(1/2) Sqrt[1-$e^2]) $r^2 - 1 / ($a^(1/2) $mu^(1/2)) Log[$a (1-$e^2) /$r] - 1 / ($a^(1/2) $mu^(1/2) Sqrt[1-$e^2]) (PolyLog[2, $e/(1 + Sqrt[1-$e^2]) Exp[-I $E]] + PolyLog[2, $e/(1 + Sqrt[1-$e^2]) Exp[I $E]])};
DecomposeDelaunayImpl[$phi / $r] := {0, 1 / ($a^(1/2) $mu^(1/2)) $r - $a^(1/2) $mu^(-1/2) (PolyLog[2, $e/(1 + Sqrt[1-$e^2]) Exp[-I $E]] + PolyLog[2, $e/(1 + Sqrt[1 -$e^2]) Exp[I $E]])};

(*
	Int[phi^m f dl]
	u = phi               dv = phi^(m-1) f = A + L[B]
	du = phi'              v = l A + n B
	Int[phi^m f dl] = phi l A + n phi B - Int[phi' A l dl] - n Int[phi' B dl]
	n phi^m f = L[phi l A] + n L[phi B] - n phi' A l - n^2 phi' B
	n phi^m f = n phi' A l + n A phi + n L[phi B] - n phi' A l - n^2 phi' B
	phi^m f = L[phi B] + A phi - n phi' B 
*)

(* phi^n r^k R*)
DecomposeDelaunayImpl[Power[$phi, m_.] func_.] := Module[{dv, A, B},
	dv = $phi^(m-1) func;
	{A, B} = DecomposeDelaunay[dv];
	{0, B $phi} + A DecomposeDelaunayImpl[$phi] - DecomposeDelaunay[$a^(-3/2) $mu^(1/2) Dt[$phi, $ell] B //Expand]
];

(* Dilogarithm *)
AllTozE[expr_] := ((((((expr /.
	$phi -> $f - $ell )/.
	$f -> I Log[(-$e zE + 1 + Sqrt[1-$e^2]) / ((1 + Sqrt[1-$e^2]) zE - $e)] )/.
	$E -> -I Log[zE] )/.
	$r -> $a/2 (-$e zE + 2 - $e/zE) )/.
	$R -> I $mu^(1/2) $a^(-1/2) $e (zE^2-1)/($e zE^2 - 2zE + $e) )/.
	$ell -> I($e/2(zE - 1/zE) - Log[zE]) ) //Expand;

AllTozf[expr_] := ((((((AnomaliesToRadius[expr] /.
	$phi -> $f - $ell )/.
	$ell -> I ($e Sqrt[1-$e^2] (zf^2-1)/($e zf^2 + 2zf + $e) + Log[($e zf + 1 + Sqrt[1-$e^2])/((1+Sqrt[1-$e^2])zf + $e)]) )/.
	$f -> -I Log[zf] )/.
	$E -> -I Log[($e zf + 1 + Sqrt[1-$e^2])/((1 + Sqrt[1-$e^2]) zf + $e)] )/.
	$r -> 2 $a (1-$e^2) zf/($e zf^2 + 2zf + $e) )/.
	$R -> I $e/(2 Sqrt[1-$e^2]) $mu^(1/2) $a^(-1/2) (1-zf^2)/zf ) //Expand;

DecomposeDelaunayImpl[f_. PolyLog[n_, beta_. Exp[Complex[0, a_] $E]]] := Module[{fdl, fdlzE, Tf, II, R},
	fdl = f I($e - 2 zE + $e zE^2)/(2 zE^2);
	fdlzE = fdl // AllTozE // Factor // Expand;
	Tf = If[ a == 1, fdlzE /. zE -> t, fdlzE t^(1/a-1)/a /. zE -> t^(1/a)] // Expand;
	II = PolyInt[Tf PolyLog[n, beta t] // Expand, t];
	R = II /. t -> Exp[Complex[0, a] $E];
	{0, $a^(3/2) $mu^(-1/2) R} // Expand
] /; FreeEllQ[beta] && FreeEllQ[a];

DecomposeDelaunayImpl[f_. PolyLog[n_, beta_. Exp[Complex[0, a_] $f]]] := Module[{fdl, fdlzf, Tf, II, R},
	fdl = f (-4I Sqrt[1-$e^2]^3 zf)/($e + 2 zf + $e zf^2)^2;
	fdlzf = fdl // AllTozf // Factor // Expand;
	Tf = If[ a == 1, fdlzf /. zf -> t, fdlzf t^(1/a-1)/a /. zf -> t^(1/a)] // Expand;
	II = PolyInt[Tf PolyLog[n, beta t] // Expand, t];
	R = II /. t -> Exp[Complex[0, a] $f];
	{0, $a^(3/2) $mu^(-1/2) R} // Expand
] /; FreeEllQ[beta] && FreeEllQ[a];

DecomposeDelaunayImpl[f_. Log[1 - beta_. Exp[Complex[0, a_] $E]]] := Module[{fdl, fdlzE, Tf, II, R},
	fdl = f I($e - 2 zE + $e zE^2)/(2 zE^2);
	fdlzE = fdl // AllTozE // Factor // Expand;
	Tf = If[ a == 1, fdlzE /. zE -> t, fdlzE t^(1/a-1)/a /. zE -> t^(1/a)] // Expand;
	II = PolyInt[Tf PolyLog[1, beta t] // Expand, t];
	R = II /. t -> Exp[Complex[0, a] $E];
	{0, $a^(3/2) $mu^(-1/2) R} // Expand
] /; FreeEllQ[beta] && FreeEllQ[a];

DecomposeDelaunayImpl[f_. Log[1 - beta_. Exp[Complex[0, a_] $f]]] := Module[{fdl, fdlzf, Tf, II, R},
	fdl = f (-4I Sqrt[1-$e^2]^3 zf)/($e + 2zf + $e zf^2)^2;
	fdlzf = fdl // AllTozf // Factor // Expand;
	Tf = If[ a == 1, fdlzf /. zf -> t, fdlzf t^(1/a-1)/a /. zf -> t^(1/a)] // Expand;
	II = PolyInt[Tf PolyLog[1, beta t] // Expand, t];
	R = II /. t -> Exp[Complex[0, a] $f];
	{0, $a^(3/2) $mu^(-1/2) R} // Expand
] /; FreeEllQ[beta] && FreeEllQ[a];


(* Int[w^q PolyLog[p, s w], w] *)
PolyInt[a_Plus, w_] := PolyInt[#, w]& /@ a;
PolyInt[c_ f_, w_] := c PolyInt[f, w] /; FreeQ[c, w];

PolyInt[Power[w_, q_.] PolyLog[p_, s_. w_], w_] := - Log[1 - s w]/(s^(q+1) (-q-1)^p) + w^(q+1)/(-q-1)^p ( -Sum[(s w)^(i-q-1)/i, {i, 1, q+1}] + Sum[(-1)^i(q+1)^(i-1)PolyLog[i, s w], {i, 1, p}] ) /; q >= 0;
PolyInt[PolyLog[p_, s_. w_], w_] := (-1)^(p+1) Log[1 - s w]/s + (-1)^p w (-1 + Sum[(-1)^i PolyLog[i, s w], {i, 1, p}]);
PolyInt[PolyLog[p_, s_. w_]/w_, w_] := PolyLog[p+1, s w];
PolyInt[PolyLog[p_, s_. w_]/w_^2, w_] := s(Log[s w] - Log[1 - s w]) - 1/w Sum[PolyLog[i, s w], {i, 1, p}];
PolyInt[Power[w_, q_.] PolyLog[p_, s_. w_], w_] := (Log[s w] - Log[1 - s w])/(s^(q+1)(-q-1)^p) + w^(q+1)/(-q-1)^p (Sum[(s w)^i/(q + 1 + i), {i, 1, -q-2}] + Sum[(-1)^i (q+1)^(i-1) PolyLog[i, s w], {i, 1, p}]) /; q <= -3;

PolyInt[Power[w_, q_.] Log[1 - s_. w_], w_] := - Log[1 - s w]/(s^(q+1) (-q-1)) + w^(q+1)/(-q-1) ( -Sum[(s w)^(i-q-1)/i, {i, 1, q+1}] - PolyLog[1, s w] ) /; q >= 0;
PolyInt[Log[1 - s_. w_], w_] := Log[1 - s w]/s - w (-1 - PolyLog[1, s w]);
PolyInt[Log[1 - s_. w_]/w_, w_] := PolyLog[2, s w];
PolyInt[Log[1 - s_. w_]/w_^2, w_] := s(Log[s w] - Log[1 - s w]) - 1/w PolyLog[1, s w];
PolyInt[Power[w_, q_.] Log[1 - s_. w_], w_] := (Log[s w] - Log[1 - s w])/(s^(q+1)(-q-1)) + w^(q+1)/(-q-1) (Sum[(s w)^i/(q + 1 + i), {i, 1, -q-2}] - PolyLog[1, s w]) /; q <= -3;

DecomposeDelaunayImpl[Log[Exp[I $E]]] := -I DecomposeDelaunayImpl[$E];
DecomposeDelaunayImpl[f_. $E] := Module[{f0, f1},
	{f0, f1} = DecomposeDelaunay[f];
	{$E f0, $E f1 // Expand} + DecomposeDelaunay[$mu^(1/2) $a^(1/2) f1/$r // Expand];
];

(* Log[p/r] *)
(*
	Int[Log[p/r] f dl]
	u = Log[p/r]               dv = f = A + L[B]
	du = -R/(n r)               v = l A + n B
	Int[Log[p/r] f dl] = Log[p/r] l A + n Log[p/r] B + Int[R/(n r) A l dl] + Int[R/r B dl]
	n Log[p/r] f = L[Log[p/r] l A] + n L[Log[p/r] B] + R/r A l + n R/r B
	n Log[p/r] f = - R/r A l + n A Log[p/r] + n L[Log[p/r] B] + R/r A l + n R/r B
	Log[p/r] f = L[Log[p/r] B] + A Log[p/r] + R/r B
*)
DecomposeDelaunayImpl[Log[$a/$r - $a $e^2/$r]] := DecomposeDelaunayImpl[Log[$a(1-$e^2)/$r]];
DecomposeDelaunayImpl[Log[$a (1 - $e^2)/$r]] := {-1 + Sqrt[1 - $e^2] + Log[2/(1 + Sqrt[1 - $e^2]) (1 - $e^2)], Sqrt[1 - $e^2] $a^(3/2) $mu^(-1/2) $phi - 2 $a/$mu $R $r + Log[2/(1 + Sqrt[1 - $e^2]) (1 - $e^2)] $a $mu^(-1) $R $r - $a/$mu $R $r Log[$a(1-$e^2)/$r] + I $a^(3/2) $mu^(-1/2) (PolyLog[2, $e/(1 + Sqrt[1 - $e^2]) Exp[-I $E]] - PolyLog[2, $e/(1 + Sqrt[1 - $e^2]) Exp[I $E]])};

DecomposeDelaunayImpl[f_ Log[$a/$r - $a $e^2/$r]] := DecomposeDelaunayImpl[f Log[$a(1 - $e^2)/$r]];
DecomposeDelaunayImpl[f_ Log[$a(1 - $e^2)/$r]] := Module[{A, B},
	{A, B} = DecomposeDelaunayImpl[f];
	{0, B Log[$a/$r-$a $e^2/$r]} + A DecomposeDelaunayImpl[Log[$a/$r-$a $e^2/$r]] + DecomposeDelaunay[$R/$r B // Expand]
];
DecomposeDelaunayImpl[f_. Power[Log[$a/$r - $a $e^2/$r], ga_]] := Module[{A, B},
	{A, B} = DecomposeDelaunayImpl[f Log[$a/$r - $a $e^2/$r]^(ga-1)];
	{0, B Log[$a/$r-$a $e^2/$r]} + A DecomposeDelaunayImpl[Log[$a/$r-$a $e^2/$r]] + DecomposeDelaunay[$R/$r B // Expand]
];

DecomposeDelaunayImpl[c_.] := {c, 0} /; FreeEllQ[c];

*)

End[]
 
EndPackage[]

